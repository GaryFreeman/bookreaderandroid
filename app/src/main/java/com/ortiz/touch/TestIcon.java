package com.ortiz.touch;

import android.graphics.Rect;

public class TestIcon
{
	protected String JSONTest;
	protected String testType;
	protected Rect position;
	protected int page;
	protected String title;
	protected int index;

	public TestIcon(String JSONTest, String testType, Rect position)
	{
		this.JSONTest = JSONTest;
		this.testType = testType;
		this.position = position;
	}

	public String getJSONTest()
	{
		return JSONTest;
	}
	public String getTestType()
	{
		return testType;
	}
	public Rect getPosition()
	{
		return position;
	}
	public int getPage()
	{
		return page;
	}
	public String getTitle()
	{
		return title;
	}
	public int getIndex()
	{
		return index;
	}

	public void setJSONTest(String JSONTest)
	{
		this.JSONTest = JSONTest;
	}
	public void setTestType(String testType)
	{
		this.testType = testType;
	}
	public void setPosition(Rect position)
	{
		this.position = position;
	}
	public void setPage(int page)
	{
		this.page = page;
	}
	public void setTitle(String title)
	{
		this.title = title;
	}
	public void setIndex(int index)
	{
		this.index = index;
	}
}

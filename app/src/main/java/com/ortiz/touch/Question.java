package com.ortiz.touch;

import java.util.Collection;
import java.util.Map;

public class Question
{
	protected String title;
	protected Collection<Map<String, Boolean>> answers;

	public Question(String title, Collection<Map<String, Boolean>> answers)
	{
		this.title = title;
		this.answers = answers;
	}

	public String getTitle()
	{
		return title;
	}
	public Collection<Map<String, Boolean>> getAnswers()
	{
		return answers;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}
	public void setAnswers(Collection<Map<String, Boolean>> answers)
	{
		this.answers = answers;
	}
}

package com.ortiz.touch;

import java.util.Collection;

public class Test
{
	protected Collection<Question> questions;

	public Test(Collection<Question> question)
	{
		this.questions = question;
	}

	public Collection<Question> getQuestions()
	{
		return questions;
	}

	public void setQuestions(Collection<Question> questions)
	{
		this.questions = questions;
	}
}

package com.ortiz.touch;

import android.graphics.Rect;
import android.media.MediaPlayer;

public class DiktorIcon
{
	protected String fileName;
	protected Rect position;
	protected MediaPlayer player = null;

	public DiktorIcon(String fileName, Rect position)
	{
		this.fileName = fileName;
		this.position = position;
	}

	public String getFileName()
	{
		return fileName;
	}
	public Rect getPosition()
	{
		return position;
	}
	public MediaPlayer getPlayer()
	{
		return player;
	}

	public void setFileName(String fileName)
	{
		this.fileName = fileName;
	}
	public void setPosition(Rect position)
	{
		this.position = position;
	}
	public void setPlayer(MediaPlayer player)
	{
		this.player = player;
	}
}

package ru.maxis_studio.book_reader;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.support.design.widget.NavigationView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ortiz.touch.DiktorIcon;
import com.ortiz.touch.TestIcon;
import com.ortiz.touch.TouchImageView;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import org.json.JSONException;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BookViewActivity extends AppCompatActivity implements ListView.OnItemClickListener,
	SeekBar.OnSeekBarChangeListener, NavigationView.OnNavigationItemSelectedListener,
	SlidingUpPanelLayout.PanelSlideListener
{
	MyViewPager pager;
	PagerAdapter pagerAdapter;
	List pages = new ArrayList();
	Map<Integer, Integer> tableOfContent = new HashMap<>();
	DrawerLayout drawerLayout;
	ActionBarDrawerToggle drawerToggle;
	EditText currentPageNumber;
	SlidingUpPanelLayout pageNavigationPanel;
	ImageView bottomSheetIcon;
	static public List tableOfContents = new ArrayList();

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_book_view);

		setSupportActionBar((Toolbar)findViewById(R.id.actionBarToolbar));

		ParsePageConfig();
		loadIconsFromJSON();

		pager = (MyViewPager)findViewById(R.id.pager);
		drawerLayout = (DrawerLayout)findViewById(R.id.drawerLayout);
		currentPageNumber = (EditText)findViewById(R.id.currentPageNumber);
		pageNavigationPanel = (SlidingUpPanelLayout)findViewById(R.id.pageNavigation);

		pageNavigationPanel.setPanelSlideListener(this);

		currentPageNumber.setImeActionLabel(getResources().getString(R.string.go), KeyEvent.KEYCODE_ENTER);
		TextView.OnEditorActionListener currentPageListener = new TextView.OnEditorActionListener(){
			public boolean onEditorAction(TextView exampleView, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_NULL && event.getAction() == KeyEvent.ACTION_DOWN) {
					pager.setCurrentItem(Integer.parseInt(currentPageNumber.getText().toString()) - 1);
				}

				return true;
			}
		};
		currentPageNumber.setOnEditorActionListener(currentPageListener);
		currentPageNumber.setSelection(currentPageNumber.getText().length());

		final SeekBar pageSlider = (SeekBar)findViewById(R.id.pageSlider);
		pageSlider.setOnSeekBarChangeListener(this);
		pageSlider.setMax(pages.size() - 1);

		TextView pagesCount = (TextView)findViewById(R.id.pagesCount);
		pagesCount.setText(String.valueOf(pages.size()));

		final FragmentManager fragmentManager = getSupportFragmentManager();
		pagerAdapter = new MyFragmentPagerAdapter(fragmentManager);
		pager.setAdapter(pagerAdapter);
		pager.addOnPageChangeListener(new OnPageChangeListener() {
			protected int oldPosition = 0;

			@Override
			public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
			{
			}

			@Override
			public void onPageSelected(int position)
			{
				if (position != oldPosition) {
					Fragment fragment = fragmentManager.findFragmentByTag(getFragmentTag(pager.getId(), oldPosition));
					if (fragment != null && fragment.getView() != null) {
						TouchImageView image = (TouchImageView)fragment.getView().findViewById(R.id.image);
						image.StopDiktor();

						currentPageNumber.setText(String.valueOf(position + 1));
						currentPageNumber.setSelection(currentPageNumber.getText().length());
						pageSlider.setProgress(position);

						oldPosition = position;
					}
				}
			}

			@Override
			public void onPageScrollStateChanged(int state)
			{
			}
		});

		int i = 0;
		for (Object p : pages) {
			Map<String, Object> page = (Map<String, Object>)p;
			String contentItem = (String)page.get("content-item");
			String contentSection = (String)page.get("content-section");

			if (contentItem != null || contentSection != null) {
				Map<String, String> item = new HashMap<>();

				if (contentSection != null) {
					item.put("isSubHeader", "true");
					item.put("title", contentSection);
					item.put("page", "");
					tableOfContents.add(item);

					item = new HashMap<>();
				}

				if (contentItem != null) {
					item.put("isSubHeader", "false");
					item.put("title", contentItem);
					item.put("page", String.valueOf(i));
				}

				tableOfContents.add(item);
			}

			++i;
		}

		ActionBar actionBar = getSupportActionBar();
		if (actionBar != null) {
			actionBar.setDisplayHomeAsUpEnabled(true);
		}

		drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.drawer_open, R.string.drawer_close);
		drawerToggle.setDrawerIndicatorEnabled(true);
		drawerLayout.setDrawerListener(drawerToggle);

		NavigationView navigationView = (NavigationView)findViewById(R.id.navigation);
		navigationView.setNavigationItemSelectedListener(this);

		final SharedPreferences bookmarksPrefs = getSharedPreferences("bookmarks", Context.MODE_PRIVATE);
		final SharedPreferences.Editor editor = bookmarksPrefs.edit();
		FloatingActionButton bookmark = (FloatingActionButton)findViewById(R.id.add_bookmark);
		bookmark.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				int bookmarksCount = bookmarksPrefs.getInt("count", 0);
				int pageNumber = pager.getCurrentItem();

				for (int i = 0; i < bookmarksCount; ++i) {
					if (bookmarksPrefs.getInt("bookmark_" + i, -1) == pageNumber) {
						Toast.makeText(BookViewActivity.this, R.string.bookmark_already_added, Toast.LENGTH_SHORT).show();
						return;
					}
				}

				editor.putInt("count", bookmarksCount + 1);
				editor.putInt("bookmark_" + bookmarksCount, pageNumber);
				editor.apply();

				Toast.makeText(BookViewActivity.this, R.string.bookmark_added, Toast.LENGTH_SHORT).show();
			}
		});

		bottomSheetIcon = (ImageView)findViewById(R.id.bottom_sheet_icon);
	}

	private void loadIconsFromJSON()
	{
		String rawEditor = readRawTextFile(this, R.raw.editor);
		String rawTests = readRawTextFile(this, R.raw.tests);

		JSONParser parser = new JSONParser();

		try {
			JSONObject editor = (JSONObject)parser.parse(rawEditor);
			JSONArray tests = (JSONArray)parser.parse(rawTests);

			JSONArray pageAnnos = (JSONArray)editor.get("pageAnnos");
			int j = 0;
			for (Object p : pageAnnos) {
				JSONArray pageIcons = (JSONArray)p;
				for (Object i : pageIcons) {
					JSONObject icon = (JSONObject)i;

					String type = (String)icon.get("annotype");
					if (type.equals("com.mobiano.flipbook.pageeditor.TAnnoImage")) {
						JSONObject location = (JSONObject)icon.get("location");
						JSONObject action = (JSONObject)icon.get("action");

						double x = Double.parseDouble((String)location.get("x"));
						double y = Double.parseDouble((String)location.get("y"));
						double width = Double.parseDouble((String)location.get("width"));
						double height = Double.parseDouble((String) location.get("height"));
						int pageWidth = Integer.parseInt((String) location.get("pageWidth"));
						int pageHeight = Integer.parseInt((String)location.get("pageHeight"));

						x *= pageWidth;
						y *= pageHeight;
						width *= pageWidth;
						height *= pageHeight;

						String actionType = (String)action.get("actionType");

						if (actionType.equals("com.mobiano.flipbook.pageeditor.TAnnoActionJavascript")) {
							// Тест

							String script = (String) action.get("scriptFun");
							int testIndex = Integer.parseInt(script.substring(10, script.length() - 2));

							JSONArray questions = null;
							JSONObject test = null;
							int i2 = 0;
							for (Object t : tests) {
								if (testIndex == i2) {
									test = (JSONObject) t;
									questions = (JSONArray) test.get("questions");
									break;
								}

								++i2;
							}

							if (questions != null) {
								Map<String, Object> iconEntry = new HashMap<>();
								iconEntry.put("x", String.valueOf((int) x));
								iconEntry.put("y", String.valueOf((int) y));
								iconEntry.put("width", String.valueOf((int) width));
								iconEntry.put("height", String.valueOf((int) height));
								iconEntry.put("type", "test");
								iconEntry.put("test-type", test.get("test-type"));
								iconEntry.put("title", test.get("title"));
								iconEntry.put("questions", questions);
								Map<String, Object> page = (Map<String, Object>) pages.get(j);
								List<Object> icons = (List<Object>) page.get("icons");

								icons.add(iconEntry);
							}
						} else if (actionType.equals("com.mobiano.flipbook.pageeditor.TAnnoActionPlayAudio")) {
							// Диктор

							String audioUrl = (String) action.get("audioURL");
							String fileName = audioUrl.substring(17, audioUrl.length());

							Map<String, Object> iconEntry = new HashMap<>();
							iconEntry.put("x", String.valueOf((int) x));
							iconEntry.put("y", String.valueOf((int) y));
							iconEntry.put("width", String.valueOf((int) width));
							iconEntry.put("height", String.valueOf((int) height));
							iconEntry.put("type", "diktor");
							iconEntry.put("filename", fileName);

							Map<String, Object> page = (Map<String, Object>) pages.get(j);
							List<Object> icons = (List<Object>) page.get("icons");
							icons.add(iconEntry);
						}
					}
				}

				++j;
			}
		} catch (ParseException ex) {
			Log.e("BookReader", ex.toString());
			ex.printStackTrace();
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		return drawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState)
	{
		super.onPostCreate(savedInstanceState);
		drawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig)
	{
		super.onConfigurationChanged(newConfig);
		drawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public void onItemClick(AdapterView parent, View view, int position, long id)
	{
		pager.setCurrentItem(tableOfContent.get(position));
		drawerLayout.closeDrawers();
	}

	private void ParsePageConfig()
	{
		XmlPullParser xml = getResources().getXml(R.xml.pages);

		// Парсим файл pages.xml
		try {
			boolean inPages = false, inIcons = false, inQuestions = false, inAnswers = false;
			Map<String, Object> icon = null, page = null, question = null, answer = null;
			String lastIconAttribute = null, lastQuestionAttribute = null;

			while (xml.getEventType() != XmlPullParser.END_DOCUMENT) {
				switch (xml.getEventType()) {
					case XmlPullParser.START_TAG:
						if (xml.getName().equals("pages") && xml.getDepth() == 1) {
							inPages = true;
						} else if (xml.getName().equals("page") && xml.getDepth() == 2 && inPages) {
							page = new HashMap<>();

							for (int i = 0; i < xml.getAttributeCount(); ++i) {
								page.put(xml.getAttributeName(i), xml.getAttributeValue(i));
							}

							pages.add(page);
						} else if (xml.getDepth() == 3 && page != null) {
							if (xml.getName().equals("icons")) {
								inIcons = true;
								page.put("icons", new ArrayList<Object>());
							}
						} else if (xml.getName().equals("icon") && xml.getDepth() == 4 && inIcons) {
							icon = new HashMap<>();

							for (int i = 0; i < xml.getAttributeCount(); ++i) {
								icon.put(xml.getAttributeName(i), xml.getAttributeValue(i));
							}

							((ArrayList<Object>)page.get("icons")).add(icon);
						} else if (xml.getDepth() == 5 && icon != null) {
							if (xml.getName().equals("questions")) {
								inQuestions = true;
								icon.put("questions", new ArrayList<Object>());
							} else {
								lastIconAttribute = xml.getName();
							}
						} else if (xml.getName().equals("question") && xml.getDepth() == 6 && inQuestions) {
							question = new HashMap<>();

							for (int i = 0; i < xml.getAttributeCount(); ++i) {
								question.put(xml.getAttributeName(i), xml.getAttributeValue(i));
							}

							((ArrayList<Object>)icon.get("questions")).add(question);
						} else if (xml.getDepth() == 7 && question != null) {
							if (xml.getName().equals("answers")) {
								inAnswers = true;
								question.put("answers", new ArrayList<>());
							} else {
								lastQuestionAttribute = xml.getName();
							}
						} else if (xml.getName().equals("answer") && xml.getDepth() == 8 && inAnswers) {
							answer = new HashMap<>();

							for (int i = 0; i < xml.getAttributeCount(); ++i) {
								answer.put(xml.getAttributeName(i), xml.getAttributeValue(i));
							}

							((ArrayList<Object>)question.get("answers")).add(answer);
						}
						break;
					case XmlPullParser.TEXT:
						if (lastIconAttribute != null) {
							icon.put(lastIconAttribute, xml.getText());
							lastIconAttribute = null;
						} else if (lastQuestionAttribute != null) {
							question.put(lastQuestionAttribute, xml.getText());
							lastQuestionAttribute = null;
						} else if (answer != null) {
							answer.put("text", xml.getText());
							answer = null;
						}
						break;
					case XmlPullParser.END_TAG:
						if (xml.getName().equals("pages") && xml.getDepth() == 1 && inPages) {
							inPages = false;
						} else if (xml.getName().equals("page") && xml.getDepth() == 2 && page != null) {
							page = null;
						} else if (xml.getName().equals("icons") && xml.getDepth() == 3 && inIcons) {
							inIcons = false;
						} else if (xml.getName().equals("icon") && xml.getDepth() == 4 && icon != null) {
							icon = null;
						} else if (xml.getName().equals("questions") && xml.getDepth() == 5 && inQuestions) {
							inQuestions = false;
						} else if (xml.getName().equals("question") && question != null) {
							question = null;
						} else if (xml.getName().equals("answers") && xml.getDepth() == 7 && inAnswers) {
							inAnswers = false;
						} else if (xml.getName().equals("answer") && answer != null) {
							answer = null;
						}
						break;
				}

				xml.next();
			}
		} catch (XmlPullParserException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	private String getFragmentTag(int viewPagerId, int fragmentPosition)
	{
		return "android:switcher:" + viewPagerId + ":" + fragmentPosition;
	}

	private class MyFragmentPagerAdapter extends FragmentPagerAdapter
	{

		public MyFragmentPagerAdapter(FragmentManager fm)
		{
			super(fm);
		}

		@Override
		public Fragment getItem(int position)
		{
			return PageFragment.newInstance((Map)pages.get(position));
		}

		@Override
		public int getCount()
		{
			return pages.size();
		}
	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
	{
		currentPageNumber.setText(String.valueOf(progress + 1));
		currentPageNumber.setSelection(currentPageNumber.getText().length());
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar)
	{
	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar)
	{
		pager.setCurrentItem(seekBar.getProgress());
	}

	@Override
	public boolean onNavigationItemSelected(MenuItem item)
	{
		Intent intent;
		JSONArray json;

		switch (item.getItemId()) {
			/*case R.id.page_navigation:
				pageNavigationPanel.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
				break;*/
			/*case R.id.table_of_content:
				intent = new Intent(this, TableOfContentsActivity.class);
				startActivityForResult(intent, 1);
				break;*/
			case R.id.about_authors:
				intent = new Intent(this, AboutAuthorsActivity.class);
				startActivity(intent);
				break;
/*			case R.id.annotation:
				intent = new Intent(this, AnnotationActivity.class);
				startActivity(intent);
				break;*/
			case R.id.notes:
				intent = new Intent(this, NotesActivity.class);
				startActivity(intent);
				break;
			case R.id.bookmarks:
				intent = new Intent(this, BookmarksActivity.class);
				startActivityForResult(intent, 1);
				break;
			case R.id.controls_list:
				intent = new Intent(this, TestsActivity.class);

				json = new JSONArray(pages);

				intent.putExtra("isControl", true);
				intent.putExtra("pagesConfig", json.toJSONString());

				startActivity(intent);
				break;
			case R.id.self_controls_list:
				intent = new Intent(this, TestsActivity.class);

				json = new JSONArray(pages);

				intent.putExtra("isControl", false);
				intent.putExtra("pagesConfig", json.toJSONString());

				startActivity(intent);
				break;
		}

		drawerLayout.closeDrawers();

		return false;
	}

	private void ClearCurrentPageNumberFieldFocus()
	{
		currentPageNumber.clearFocus();

		InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(currentPageNumber.getWindowToken(), 0);
	}

	@Override
	public void onPanelSlide(View panel, float slideOffset)
	{
	}

	@Override
	public void onPanelCollapsed(View panel)
	{
		ClearCurrentPageNumberFieldFocus();
		bottomSheetIcon.setImageResource(android.R.drawable.arrow_up_float);
	}

	@Override
	public void onPanelExpanded(View panel)
	{
		bottomSheetIcon.setImageResource(android.R.drawable.arrow_down_float);
	}

	@Override
	public void onPanelAnchored(View panel)
	{
	}

	@Override
	public void onPanelHidden(View panel)
	{
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		switch (requestCode) {
			case 1:
				if (resultCode == RESULT_OK) {
					int page = data.getIntExtra("page", 0);
					pager.setCurrentItem(page);
				}
				break;
		}
	}

	public static String readRawTextFile(Context ctx, int resId)
	{
		InputStream inputStream = ctx.getResources().openRawResource(resId);

		InputStreamReader inputreader = new InputStreamReader(inputStream);
		BufferedReader buffreader = new BufferedReader(inputreader);
		String line;
		StringBuilder text = new StringBuilder();

		try {
			while ((line = buffreader.readLine()) != null) {
				text.append(line);
				text.append('\n');
			}
		} catch (IOException e) {
			return null;
		}

		return text.toString();
	}
}

package ru.maxis_studio.book_reader;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class TestActivity extends AppCompatActivity implements View.OnClickListener
{
	protected int currentIndex = 0, rightCount = 0, notRightCount = 0;
	protected List<Map<String, Object>> questions = new ArrayList<>();

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_test);

		setSupportActionBar((Toolbar)findViewById(R.id.actionBarToolbar));

		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);

		Intent intent = getIntent();

		try {
			JSONParser parser = new JSONParser();
			JSONArray test = (JSONArray)parser.parse(intent.getStringExtra("test"));

			questions = test;
		} catch (ParseException ex) {
			ex.printStackTrace();
		}

		LoadQuestion(0);

		findViewById(R.id.next).setOnClickListener(this);
		findViewById(R.id.finishButton).setOnClickListener(this);

		if (actionBar != null) {
			String testType = intent.getStringExtra("testType");
			if (testType.equals("control")) {
				actionBar.setTitle(R.string.control);
			} else {
				actionBar.setTitle(R.string.self_control);
			}
		}
	}

	protected void LoadQuestion(int index)
	{
		currentIndex = index;
		Map<String, Object> question = questions.get(currentIndex);

		TextView questionTitle = (TextView)findViewById(R.id.title);
		questionTitle.setText(Html.fromHtml((String)question.get("title")));

		RadioGroup answersRadioGroup = (RadioGroup)findViewById(R.id.answers);
		answersRadioGroup.removeAllViews();
		answersRadioGroup.clearCheck();
		for (Map<String, String> answer : (List<Map<String, String>>)question.get("answers")) {
			RadioButton answerView = new RadioButton(this);
			answerView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.WRAP_CONTENT));
			answerView.setId(IDGenerator.generateViewId());

			answerView.setText(answer.get("text"));
			answersRadioGroup.addView(answerView);
		}
	}

	protected void CheckAnswer()
	{
		boolean right = false;

		RadioGroup answersRadioGroup = (RadioGroup)findViewById(R.id.answers);

		int answerId = answersRadioGroup.getCheckedRadioButtonId();
		if (answerId != -1) {
			String answerText = ((RadioButton)findViewById(answerId)).getText().toString();
			for (Map<String, String> answer : (List<Map<String, String>>)questions.get(currentIndex).get("answers")) {
				if (answerText.equals((String)answer.get("text"))) {
					if (((String)answer.get("right")).equalsIgnoreCase("true")) {
						right = true;
					}

					break;
				}
			}
		}

		if (right) {
			++rightCount;
		} else {
			++notRightCount;
		}
	}

	@Override
	public void onClick(View v)
	{
		CheckAnswer();

		switch (v.getId()) {
			case R.id.next:
				if (currentIndex + 1 < questions.size()) {
					LoadQuestion(currentIndex + 1);
				} else {
					CompleteTest();
				}
				break;
			case R.id.finishButton:
				finish();
				break;
		}
	}

	protected void CompleteTest()
	{
		findViewById(R.id.test).setVisibility(View.GONE);
		TextView result = (TextView)findViewById(R.id.result);

		if (rightCount + notRightCount == 1) {
			if (rightCount == 1) {
				result.setText(R.string.right);
			} else {
				result.setText(R.string.not_right);
			}
		} else {
			result.setText(getResources().getString(R.string.result_points, rightCount));
		}

		result.setVisibility(View.VISIBLE);
		Button finishButton = (Button)findViewById(R.id.finishButton);
		finishButton.setVisibility(View.VISIBLE);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		if (item.getItemId() == android.R.id.home) {
			// Костыль. Стандартный обработчик кнопки Назад возвращается с анимацией открытия нового Activity, а не
			// закрытия

			finish();

			return true;
		}

		return false;
	}
}

package ru.maxis_studio.book_reader;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class NotesActivity extends AppCompatActivity
{
	private NotesAdapter adapter;
	private View noNotes;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_notes);

		setSupportActionBar((Toolbar) findViewById(R.id.actionBarToolbar));
		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setTitle(R.string.notes);

		final SharedPreferences prefs = getSharedPreferences("notes", Context.MODE_PRIVATE);

		final RecyclerView notesList = (RecyclerView)findViewById(R.id.notesList);
		noNotes = findViewById(R.id.noNotes);

		notesList.setHasFixedSize(true);

		LinearLayoutManager layoutManager = new LinearLayoutManager(this);
		notesList.setLayoutManager(layoutManager);

		adapter = new NotesAdapter(prefs);
		notesList.setAdapter(adapter);

		ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0,
			ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

			@Override
			public void onSwiped(RecyclerView.ViewHolder vh, int swipeDir)
			{
				NotesAdapter.ViewHolder viewHolder = (NotesAdapter.ViewHolder)vh;
				int notesCount = prefs.getInt("notesCount", 1);
				int deletingPosition = viewHolder.getAdapterPosition();

				SharedPreferences.Editor editor = prefs.edit();

				editor.putInt("notesCount", notesCount - 1);
				editor.remove("note_" + deletingPosition + "_title");
				editor.remove("note_" + deletingPosition + "_text");

				for (int i = deletingPosition + 1; i < notesCount; ++i) {
					String noteTitle = prefs.getString("note_" + i + "_title",
						getResources().getString(R.string.unnamed));
					String noteText = prefs.getString("note_" + i + "_text", "");

					editor.putString("note_" + (i - 1) + "_title", noteTitle);
					editor.putString("note_" + (i - 1) + "_text", noteText);
				}

				editor.commit();

				adapter.notifyItemRemoved(deletingPosition);

				if (adapter.getItemCount() == 0) {
					noNotes.setVisibility(View.VISIBLE);
				}
			}

			@Override
			public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder,
								  RecyclerView.ViewHolder target)
			{
				return false;
			}

		};

		ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
		itemTouchHelper.attachToRecyclerView(notesList);

		if (adapter.getItemCount() == 0) {
			noNotes.setVisibility(View.VISIBLE);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.notes_menu, menu);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId()) {
			case android.R.id.home:
				// Костыль. Стандартный обработчик кнопки Назад возвращается с анимацией открытия нового Activity, а не
				// закрытия

				finish();
				break;
			case R.id.add_note:
				Intent intent = new Intent(this, NoteActivity.class);
				startActivity(intent);
				break;

		}

		return true;
	}

	@Override
	protected void onResume()
	{
		super.onResume();

		adapter.notifyDataSetChanged();

		if (adapter.getItemCount() != 0) {
			noNotes.setVisibility(View.GONE);
		}
	}

	private class NotesAdapter extends RecyclerView.Adapter<NotesAdapter.ViewHolder>
	{
		private SharedPreferences sharedPreferences;

		public class ViewHolder extends RecyclerView.ViewHolder
		{
			public TextView noteTitle;
			public TextView noteText;

			public ViewHolder(View v)
			{
				super(v);

				noteTitle = (TextView)v.findViewById(R.id.noteTitle);
				noteText = (TextView)v.findViewById(R.id.noteText);
			}
		}

		public NotesAdapter(SharedPreferences sharedPreferences)
		{
			this.sharedPreferences = sharedPreferences;
		}

		@Override
		public NotesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
		{
			View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.note_item, parent, false);
			final ViewHolder viewHolder = new ViewHolder(view);

			view.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					Intent intent = new Intent(v.getContext(), NoteActivity.class);
					intent.putExtra("noteId", viewHolder.getAdapterPosition());
					startActivity(intent);
				}
			});

			return viewHolder;
		}

		@Override
		public void onBindViewHolder(ViewHolder holder, int position)
		{
			String noteTitle = sharedPreferences.getString("note_" + position + "_title",
				getResources().getString(R.string.unnamed));
			String noteText = sharedPreferences.getString("note_" + position + "_text", "");

			holder.noteTitle.setText(noteTitle);
			holder.noteText.setText(noteText);
		}

		@Override
		public int getItemCount()
		{
			return sharedPreferences.getInt("notesCount", 0);
		}
	}
}

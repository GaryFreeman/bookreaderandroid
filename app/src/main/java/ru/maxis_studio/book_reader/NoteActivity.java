package ru.maxis_studio.book_reader;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

public class NoteActivity extends AppCompatActivity
{
	private int noteId = -1;
	private EditText noteTitle;
	private EditText noteText;
	private SharedPreferences prefs;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_note);

		noteTitle = (EditText)findViewById(R.id.note_title);
		noteText = (EditText)findViewById(R.id.note_text);
		prefs = getSharedPreferences("notes", Context.MODE_PRIVATE);

		setSupportActionBar((Toolbar)findViewById(R.id.actionBarToolbar));
		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);

		Intent intent = getIntent();
		noteId = intent.getIntExtra("noteId", -1);

		if (noteId != -1) {
			actionBar.setTitle(R.string.edit_note);

			String title = prefs.getString("note_" + noteId + "_title", getResources().getString(R.string.unnamed));
			String text = prefs.getString("note_" + noteId + "_text", "");

			noteTitle.setText(title);
			noteText.setText(text);
		} else {
			actionBar.setTitle(R.string.add_note);
		}
	}

	private void saveNote()
	{
		String title = noteTitle.getText().toString();
		String text = noteText.getText().toString();

		if (!title.isEmpty() && !text.isEmpty()) {
			int notesCount = prefs.getInt("notesCount", 0);
			SharedPreferences.Editor editor = prefs.edit();

			if (noteId == -1) {
				noteId = notesCount;
				editor.putInt("notesCount", notesCount + 1);
			}

			editor.putString("note_" + noteId + "_title", title);
			editor.putString("note_" + noteId + "_text", text);

			editor.commit();

			Toast.makeText(NoteActivity.this, R.string.note_saved, Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId()) {
			case android.R.id.home:
				saveNote();

				// Костыль. Стандартный обработчик кнопки Назад возвращается с анимацией открытия нового Activity, а не
				// закрытия
				finish();
				break;
		}

		return true;
	}

	@Override
	public void onBackPressed()
	{
		saveNote();

		finish();
	}
}

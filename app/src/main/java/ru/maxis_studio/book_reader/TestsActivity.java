package ru.maxis_studio.book_reader;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ortiz.touch.DiktorIcon;
import com.ortiz.touch.Test;
import com.ortiz.touch.TestIcon;

import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class TestsActivity extends AppCompatActivity
{
	private TestsAdapter adapter;
	private List pagesConfig;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tests);

		setSupportActionBar((Toolbar) findViewById(R.id.actionBarToolbar));
		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);

		Intent intent = getIntent();
		boolean isControl = intent.getBooleanExtra("isControl", true);
		if (isControl) {
			actionBar.setTitle(R.string.controls_list);
		} else {
			actionBar.setTitle(R.string.self_controls_list);
		}

		try {
			String pagesConfig = intent.getStringExtra("pagesConfig");
			JSONParser parser = new JSONParser();
			this.pagesConfig = (List)parser.parse(pagesConfig);
		} catch (ParseException ex) {
			ex.printStackTrace();
		}

		final RecyclerView bookmarksList = (RecyclerView)findViewById(R.id.testsList);

		bookmarksList.setHasFixedSize(true);

		LinearLayoutManager layoutManager = new LinearLayoutManager(this);
		bookmarksList.setLayoutManager(layoutManager);

		List<TestIcon> testIcons = new ArrayList<>();

		int j = 0;
		for (Object page : (List<Object>)pagesConfig) {
			for (Object i : (List<Object>)((Map<String, Object>)page).get("icons")) {
				Map icon = (Map)i;
				Rect position = new Rect(Integer.parseInt((String)icon.get("x")),
					Integer.parseInt((String)icon.get("y")),
					Integer.parseInt((String)icon.get("x")) + Integer.parseInt((String)icon.get("width")),
					Integer.parseInt((String)icon.get("y")) + Integer.parseInt((String)icon.get("height")));

				switch ((String)icon.get("type")) {
					case "test":
						JSONArray test = new JSONArray((List)icon.get("questions"));
						String title = (String)icon.get("title");
						TestIcon testIcon = new TestIcon(test.toJSONString(), (String)icon.get("test-type"), position);
						testIcon.setPage(j);
						testIcon.setTitle(title);
						if ((testIcon.getTestType().equals("control") && isControl) ||
							(testIcon.getTestType().equals("self_control") && !isControl)) {
							testIcons.add(testIcon);
						}
						break;
				}
			}

			++j;
		}

		adapter = new TestsAdapter(testIcons);
		bookmarksList.setAdapter(adapter);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId()) {
			case android.R.id.home:
				// Костыль. Стандартный обработчик кнопки Назад возвращается с анимацией открытия нового Activity, а не
				// закрытия

				finish();
				break;
		}

		return true;
	}

	private class TestsAdapter extends RecyclerView.Adapter<TestsAdapter.ViewHolder>
	{
		private List<TestIcon> testsIcons;

		public class ViewHolder extends RecyclerView.ViewHolder
		{
			public View view;
			public TextView title;

			public ViewHolder(View v)
			{
				super(v);

				view = v;
				title = (TextView)view.findViewById(R.id.testTitle);
			}
		}

		public TestsAdapter(List<TestIcon> testsIcons)
		{
			this.testsIcons = testsIcons;
		}

		@Override
		public TestsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
		{
			View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.test_item, parent, false);
			ViewHolder viewHolder = new ViewHolder(view);

			return viewHolder;
		}

		@Override
		public void onBindViewHolder(final ViewHolder holder, final int position)
		{
			final TestIcon icon = testsIcons.get(position);

			int resId;
			if (icon.getTestType().equals("control")) {
				resId = R.string.control_title;
			} else {
				resId = R.string.self_control_title;
			}

			String title = icon.getTitle();
			if (title == null) {
				title = String.format(holder.view.getContext().getResources().getString(resId), icon.getPage() + 1);
			}

			holder.title.setText(title);

			holder.view.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					Intent intent = new Intent(holder.view.getContext(), TestActivity.class);
					intent.putExtra("test", icon.getJSONTest());
					intent.putExtra("testType", icon.getTestType());
					holder.view.getContext().startActivity(intent);
				}
			});
		}

		@Override
		public int getItemCount()
		{
			return testsIcons.size();
		}
	}
}

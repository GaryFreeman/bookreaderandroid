package ru.maxis_studio.book_reader;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TableOfContentsActivity extends AppCompatActivity
{
	private TableOfContentsAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_table_of_contents);

		setSupportActionBar((Toolbar)findViewById(R.id.actionBarToolbar));
		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setTitle(R.string.table_of_content);

		RecyclerView tableOfContentsList = (RecyclerView)findViewById(R.id.notesList);

		tableOfContentsList.setHasFixedSize(true);

		LinearLayoutManager layoutManager = new LinearLayoutManager(this);
		tableOfContentsList.setLayoutManager(layoutManager);

		adapter = new TableOfContentsAdapter(BookViewActivity.tableOfContents);
		tableOfContentsList.setAdapter(adapter);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId()) {
			case android.R.id.home:
				// Костыль. Стандартный обработчик кнопки Назад возвращается с анимацией открытия нового Activity, а не
				// закрытия

				finish();
				break;

		}

		return true;
	}

	private class TableOfContentsAdapter extends RecyclerView.Adapter<TableOfContentsAdapter.ViewHolder>
	{
		List tableOfContents;

		public class ViewHolder extends RecyclerView.ViewHolder
		{
			public boolean isSubHeader = false;
			public View view;
			TextView title;

			public ViewHolder(View v)
			{
				super(v);

				view = v;
			}
		}

		public TableOfContentsAdapter(List tableOfContents)
		{
			this.tableOfContents = tableOfContents;

			for (int i = 0; i < tableOfContents.size(); ++i) {
				Map<String, String> item = (Map<String, String>) tableOfContents.get(i);
				String[] items = item.get("title").split("\\|");

				if (items.length > 1) {
					tableOfContents.remove(i);

					for (int j = 0; j < items.length; ++j) {
						Map<String, String> newItem = new HashMap<>(item);
						newItem.put("title", items[j]);
						tableOfContents.add(i + j, newItem);
					}
				}
			}
		}

		@Override
		public TableOfContentsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
		{
			View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.table_of_contents_item, parent, false);
			ViewHolder viewHolder = new ViewHolder(view);

			return viewHolder;
		}

		@Override
		public void onBindViewHolder(ViewHolder holder, int position)
		{
			final Map<String, String> item = (Map<String, String>)tableOfContents.get(position);
			View.OnClickListener listener;

			if (item.get("isSubHeader").equals("true")) {
				holder.isSubHeader = true;
				holder.title = (TextView)holder.view.findViewById(R.id.subHeaderTitle);
				holder.view.findViewById(R.id.title).setVisibility(View.GONE);
				holder.view.setClickable(false);
				listener = null;
				holder.view.findViewById(R.id.divider).setVisibility(View.VISIBLE);

				if (position == 0) {
					holder.view.findViewById(R.id.divider).setVisibility(View.GONE);
				} else {
					holder.view.findViewById(R.id.divider).setVisibility(View.VISIBLE);
				}
			} else {
				holder.title = (TextView)holder.view.findViewById(R.id.title);
				holder.view.findViewById(R.id.subHeaderTitle).setVisibility(View.GONE);
				holder.view.setClickable(true);
				listener = new View.OnClickListener() {
					@Override
					public void onClick(View v)
					{
						Intent result = new Intent();
						result.putExtra("page", Integer.parseInt(item.get("page")));

						setResult(Activity.RESULT_OK, result);
						finish();
					}
				};
				holder.view.findViewById(R.id.divider).setVisibility(View.GONE);
			}

			holder.view.setOnClickListener(listener);
			holder.title.setVisibility(View.VISIBLE);
			holder.title.setText(item.get("title"));
		}

		@Override
		public int getItemCount()
		{
			return tableOfContents.size();
		}
	}
}

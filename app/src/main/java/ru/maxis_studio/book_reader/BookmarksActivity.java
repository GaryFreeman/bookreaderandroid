package ru.maxis_studio.book_reader;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class BookmarksActivity extends AppCompatActivity
{
	private BookmarksAdapter adapter;
	private View noBookmarks;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bookmarks);

		setSupportActionBar((Toolbar) findViewById(R.id.actionBarToolbar));
		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setTitle(R.string.bookmarks);

		final SharedPreferences prefs = getSharedPreferences("bookmarks", Context.MODE_PRIVATE);

		final RecyclerView bookmarksList = (RecyclerView)findViewById(R.id.bookmarksList);
		noBookmarks = findViewById(R.id.noBookmarks);

		bookmarksList.setHasFixedSize(true);

		LinearLayoutManager layoutManager = new LinearLayoutManager(this);
		bookmarksList.setLayoutManager(layoutManager);

		adapter = new BookmarksAdapter(prefs);
		bookmarksList.setAdapter(adapter);

		ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0,
			ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

			@Override
			public void onSwiped(RecyclerView.ViewHolder vh, int swipeDir)
			{
				BookmarksAdapter.ViewHolder viewHolder = (BookmarksAdapter.ViewHolder)vh;
				int bookmarksCount = prefs.getInt("count", 1);
				int deletingPosition = viewHolder.getAdapterPosition();

				SharedPreferences.Editor editor = prefs.edit();

				editor.putInt("count", bookmarksCount - 1);
				editor.remove("note_" + deletingPosition + "_title");
				editor.remove("note_" + deletingPosition + "_text");

				for (int i = deletingPosition + 1; i < bookmarksCount; ++i) {
					String noteTitle = prefs.getString("note_" + i + "_title",
						getResources().getString(R.string.unnamed));
					String noteText = prefs.getString("note_" + i + "_text", "");

					editor.putString("note_" + (i - 1) + "_title", noteTitle);
					editor.putString("note_" + (i - 1) + "_text", noteText);
				}

				editor.commit();

				adapter.notifyItemRemoved(deletingPosition);

				if (adapter.getItemCount() == 0) {
					noBookmarks.setVisibility(View.VISIBLE);
				}
			}

			@Override
			public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder,
								  RecyclerView.ViewHolder target)
			{
				return false;
			}

		};

		ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
		itemTouchHelper.attachToRecyclerView(bookmarksList);

		if (adapter.getItemCount() == 0) {
			noBookmarks.setVisibility(View.VISIBLE);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId()) {
			case android.R.id.home:
				// Костыль. Стандартный обработчик кнопки Назад возвращается с анимацией открытия нового Activity, а не
				// закрытия

				finish();
				break;
		}

		return true;
	}

	@Override
	protected void onResume()
	{
		super.onResume();

		adapter.notifyDataSetChanged();

		if (adapter.getItemCount() != 0) {
			noBookmarks.setVisibility(View.GONE);
		}
	}

	private class BookmarksAdapter extends RecyclerView.Adapter<BookmarksAdapter.ViewHolder>
	{
		private SharedPreferences sharedPreferences;

		public class ViewHolder extends RecyclerView.ViewHolder
		{
			public TextView bookmarkTitle;

			public ViewHolder(View v)
			{
				super(v);

				bookmarkTitle = (TextView)v.findViewById(R.id.bookmarkTitle);
			}
		}

		public BookmarksAdapter(SharedPreferences sharedPreferences)
		{
			this.sharedPreferences = sharedPreferences;
		}

		@Override
		public BookmarksAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
		{
			View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.bookmark_item, parent, false);
			final ViewHolder viewHolder = new ViewHolder(view);

			view.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					Intent result = new Intent();
					result.putExtra("page", sharedPreferences.getInt("bookmark_" + viewHolder.getAdapterPosition(), 0));

					setResult(Activity.RESULT_OK, result);

					finish();
				}
			});

			return viewHolder;
		}

		@Override
		public void onBindViewHolder(ViewHolder holder, int position)
		{
			holder.bookmarkTitle.setText(String.format(getString(R.string.bookmark_title),
				sharedPreferences.getInt("bookmark_" + position, 0) + 1));
		}

		@Override
		public int getItemCount()
		{
			return sharedPreferences.getInt("count", 0);
		}
	}
}

package ru.maxis_studio.book_reader;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;

import com.ortiz.touch.TouchImageView;

public class MyViewPager extends ViewPager
{
	public MyViewPager(Context context)
	{
		super(context);
	}

	public MyViewPager(Context context, AttributeSet attrs)
	{
		super(context, attrs);
	}

	@Override
	protected boolean canScroll(View v, boolean checkV, int dx, int x, int y)
	{
		if (v instanceof TouchImageView) {
			return ((TouchImageView)v).canScrollHorizontallyFroyo(-dx);
		} else {
			return super.canScroll(v, checkV, dx, x, y);
		}
	}
}

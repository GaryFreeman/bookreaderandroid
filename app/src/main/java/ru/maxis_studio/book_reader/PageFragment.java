package ru.maxis_studio.book_reader;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.ortiz.touch.DiktorIcon;
import com.ortiz.touch.TestIcon;
import com.ortiz.touch.TouchImageView;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class PageFragment extends Fragment
{
	private static final String ARG_PAGE_CONFIG = "page_config";

	protected Map pageConfig;

	public static PageFragment newInstance(Map pageConfig)
	{
		Bundle args = new Bundle();
		args.putString(ARG_PAGE_CONFIG, new JSONObject(pageConfig).toJSONString());

		PageFragment fragment = new PageFragment();
		fragment.setArguments(args);

		return fragment;
	}

	public PageFragment()
	{
		// Required empty public constructor
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		if (getArguments() != null) {
			try {
				JSONParser parser = new JSONParser();
				pageConfig = (Map)parser.parse(getArguments().getString(ARG_PAGE_CONFIG));
			} catch (ParseException ex) {
				ex.printStackTrace();
			}
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.fragment_page, null);

		try {
			InputStream stream = getActivity()
				.getAssets()
				.open((String)pageConfig.get("filename"));

			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inScaled = false;
			Bitmap b = BitmapFactory.decodeStream(stream, null, options);
			BitmapDrawable d = new BitmapDrawable(b);
			d.setTargetDensity(getContext().getResources().getDisplayMetrics().densityDpi);

			TouchImageView image = (TouchImageView)view.findViewById(R.id.image);
			image.setImageDrawable(d);

			List<TestIcon> testIcons = new ArrayList<>();
			List<DiktorIcon> diktorIcons = new ArrayList<>();

			for (Object i : (List<Object>)pageConfig.get("icons")) {
				Map icon = (Map)i;
				Rect position = new Rect(
					Integer.parseInt((String)icon.get("x")),
					Integer.parseInt((String)icon.get("y")),
					Integer.parseInt((String)icon.get("x")) + Integer.parseInt((String)icon.get("width")),
					Integer.parseInt((String)icon.get("y")) + Integer.parseInt((String)icon.get("height"))
				);

				switch ((String)icon.get("type")) {
					case "test":
						JSONArray test = new JSONArray((List)icon.get("questions"));
						testIcons.add(new TestIcon(test.toJSONString(), (String)icon.get("test-type"), position));
						break;
					case "diktor":
						String fileName = (String)icon.get("filename");
						diktorIcons.add(new DiktorIcon(fileName, position));
						break;
				}
			}

			TestIcon[] testIconsArray = new TestIcon[testIcons.size()];
			DiktorIcon[] diktorIconsArray = new DiktorIcon[diktorIcons.size()];

			testIcons.toArray(testIconsArray);
			diktorIcons.toArray(diktorIconsArray);

			image.addTestIcons(testIconsArray);
			image.addDiktorIcons(diktorIconsArray);
		} catch (IOException ex) {
			Log.e("BookReader", "Изображение страницы не найдено.", ex);
		}

		return view;
	}
}
